<?php

// The list of archives to fix.
$archives = [
  'spring-2016',
  'fall-2016',
  'winter-2017',
  'spring-2017',
  'fall-2017',
  'winter-2018',
  'spring-2018',
  'fall-2018',
  'winter-2019',
  'spring-2019',
  'fall-2019',
  'winter-2020',
  'spring-2020',
  'fall-2020',
  'winter-2021',
  'spring-2021',
  'fall-2021',
  'winter-2022',
  'spring-2022',
  'fall-2022',
  'winter-2023',
  'spring-2023',
  'fall-2023',
  'winter-2024'
];

// Step through each archive.
foreach ($archives as $archive) {

  // Set the path to the archive.
  $path = '../archive-' . $archive;

  // Reset the files array.
  $files = [];

  // Get the directories recursively.
  $rdi = new RecursiveDirectoryIterator($path, FilesystemIterator::KEY_AS_PATHNAME);

  // Step through each of the directories and get the files.
  foreach (new RecursiveIteratorIterator($rdi, RecursiveIteratorIterator::SELF_FIRST) as $file => $info) {

    // If this is not a directory and not the . or .. file, then add
    // it to the files array.
    if (
      !is_dir($file) &&
      $info->getFileName() !== '.' &&
      $info->getFileName() !== '..'
    ) {
      $files[] = $file;
    }
  }

  // Step through each of the files and change the links.
  foreach ($files as $file) {

    // Load the contents of the file.
    $contents = file_get_contents($file);

    // Remove Graduate course search link.
    $contents = preg_replace(
      '/<li>\s*?<a[^>]*?>Graduate course search<\/a>.*?<\/li>/s',
      '',
      $contents
    );

    // Replace current Graduate Studies Academic Calendar.
    $contents = preg_replace(
      '/href="https:\/\/uwaterloo\.ca\/graduate-studies-academic-calendar\/?"/s',
      'href="https://uwaterloo.ca/academic-calendar/graduate-studies/catalog"',
      $contents
    );

    // Replace archives link.
    $contents = str_replace(
      'href="../../archives.html"',
      'href="https://uwaterloo.ca/academic-calendar/archives"',
      $contents
    );

    // Unlink general information and regulations.
    $contents = preg_replace(
      '/<a[^>]*?>general information and regulations section of the Graduate Studies Academic Calendar<\/a>/s',
      'general information and regulations section of the Graduate Studies Academic Calendar',
      $contents
    );

    // Unlink English language proficiency (ELP).
    $contents = preg_replace(
      '/<p><a[^>]*?>English language proficiency \(ELP\)<\/a>.*?<\/p>/s',
      'English language proficiency (ELP) (if applicable)',
      $contents
    );

    // Unlink Graduate Academic Integrity Module (Graduate AIM).
    $contents = preg_replace(
      '/<a[^>]*?>Graduate Academic Integrity Module \(Graduate AIM\)<\/a>/s',
      'Graduate Academic Integrity Module (Graduate AIM)',
      $contents
    );

    // Fix course list links for Spring 2016.
    if ($archive == 'spring-2016') {
        $contents = str_replace(
            'href="http://www.ucalendar.uwaterloo.ca/SA/GRAD/1516/',
            'href="/graduate-studies/2016/winter/www.ucalendar.uwaterloo.ca/SA/GRAD/1516/',
            $contents
        );
        $contents = str_replace(
            'href="http://uwaterloo.ca/gsac-archive/2016/winter/www.ucalendar.uwaterloo.ca/SA/GRAD/1516/',
            'href="/graduate-studies/2016/winter/www.ucalendar.uwaterloo.ca/SA/GRAD/1516/',
            $contents
        );
    }

    // Fix course list links for Fall 2016.
    if ($archive == 'fall-2016') {
      preg_match_all(
        '/<a[^>]*?href="http:\/\/www.ucalendar.uwaterloo.ca\/SA\/GRAD\/1617\/[^>]*?>(.*?)<\/a>/s',
        $contents,
        $search_replace
      );

      for ($i = 0; $i < count($search_replace[0]); $i ++) {
        $contents = str_replace(
          $search_replace[0][$i],
          $search_replace[1][$i],
          $contents
        );
      }

      $contents = preg_replace(
        '/<span>\s*?Link\(s\) to courses\s*?<\/span>/s',
        '<span> Link(s) to courses: for information about these courses, please contact Graduate Studies and Postdoctoral Affairs </span>',
        $contents
      );

    }

    // Remove link(s) to Graduate courses.
    if ($archive == 'spring-2016' || $archive == 'fall-2016') {
        $contents = preg_replace(
            '/<li>\s*<span class="field-label">\s*Link\(s\) to graduate courses.*?<\/div>\s*<\/div>/s',
            '',
            $contents
        );
    }

    // Remove login link.
    $contents = preg_replace(
      '/<div id="cas_login">.*?<\/div>/s',
      '',
      $contents
    );

    // Save the file.
    file_put_contents($file, $contents);
  }
}
