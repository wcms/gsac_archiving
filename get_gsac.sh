# Show error if there is no archive specified.
if [ $# -eq 0 ]; then
  echo "Error: You must supply a gsac site to download"
  exit 1;
fi

# Show error if there is more than one archive specified.
if [ $# -gt 1 ]; then
  echo "Error: You must supply only one gsac site to download"
  exit 1;
fi

# Before we do anything, save the current directory we are in.
gsacarchdir=$(pwd)

# Get the exclude directory and archive url, based on archive.
if [[ $1 == 'archive-winter-2024' ]]; then
  excludedirs="/graduate-studies-academic-calendar/archives,/graduate-studies-academic-calendar/archive-*"
  archive="https://uwaterloo.ca/graduate-studies-academic-calendar/"
else
  excludedirs="/graduate-studies-academic-calendar/$1/archives"
  archive="https://uwaterloo.ca/graduate-studies-academic-calendar/$1"
fi

# Set the wget command.
  wgetcmd="
  wget --trust-server-names \
    --mirror \
    --page-requisites \
    --adjust-extension \
    --convert-links \
    --domains uwaterloo.ca \
    --exclude-domains cas.uwaterloo.ca \
    --exclude-directories=$excludedirs \
    --no-parent \
    --no-host-directories \
    --quiet \
    --show-progress \
      $archive
  "

# Move up a directory so we are not putting the archive
# into the git repo.
cd ..

# If there is a directory for the archive, then remove it.
if [ -d "$1" ]; then
  rm -rf "$1"
fi

# Create a directory for the archive.
mkdir "$1"

# Change into the archive directory.
cd "$1"

# Run the wget command.
$wgetcmd

# Remove the downloaded directories.
rm -rf brand
rm -rf fonts
rm -rf favicon.ico

# Move everything to the root directory.
mv graduate-studies-academic-calendar/* .

# Remove the gsac folder.
rm -rf graduate-studies-academic-calendar

# Move back to the gasc archiving directory.
cd "$gsacarchdir"

# Run the command to fix the directories.
./fix_dirs.sh $1

# Just ensure that we are back in the gsac archiving directory.
cd "$gsacarchdir"

# Run the php script to remove the links.
php removelinks.php $1