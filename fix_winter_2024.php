<?php

/**
 * @file
 * Removing links, search and banners from previously archived sites.
 */

// Set the path to the archive.
$path = '../archive-winter-2024';

// Reset the files array.
$files = [];

// Get the directories recursively.
$rdi = new RecursiveDirectoryIterator($path, FilesystemIterator::KEY_AS_PATHNAME);

// Step through each of the directories and get the files.
foreach (new RecursiveIteratorIterator($rdi, RecursiveIteratorIterator::SELF_FIRST) as $file => $info) {

  // If this is not a directory and not the . or .. file, then add
  // it to the files array.
  if (
    !is_dir($file) &&
    $info->getFileName() !== '.' &&
    $info->getFileName() !== '..'
  ) {

    $files[] = $file;
  }
}

// Step through each of the files and change the links.
foreach ($files as $file) {

  // Load the contents of the file.
  $contents = file_get_contents($file);

  $replacements['title="Graduate Studies Academic Calendar" rel="home">'] = 'title="Graduate Studies Academic Calendar - Winter 2024 (Archive)" rel="home">';
  $replacements['Graduate Studies Academic Calendar                  </a>'] = 'Graduate Studies Academic Calendar - Winter 2024 (Archive)</a>';
  $replacements['title="" class="active">Graduate Studies Academic Calendar home</a>'] = 'title="Graduate Studies Academic Calendar - Winter 2024 (Archive)" class="active">Graduate Studies Academic Calendar home - Winter 2024 (Archive)</a>';
  $replacements['<a href="general-information-and-regulations.html">General information and regulations</a>'] = '<a href="general-information-and-regulations.html">General information and regulations - Winter 2024 (Archive)</a>';
  $replacements['<p>This is the homepage for the University of Waterloo Graduate Studies Academic Calendar. This is the archived fall 2023 version; the most up-to-date program information is available through the '] = '';
  $replacements['<a href="https://uwaterloo.ca/graduate-studies-academic-calendar/">current Graduate Studies Academic Calendar</a>.</p>'] = '';
  $replacements['<p>The program information below is valid for the <strong>winter 2024 term</strong> (January 1, 2024 - April 30, 2024).'] = '<p>The program information below was valid for the <strong>winter 2024 term</strong> (January 1, 2024 - April 30, 2024). This is the archived version; the most up-to-date program information is available through the <a href="https://uwaterloo.ca/academic-calendar/graduate-studies/catalog"> current Graduate Studies Academic Calendar</a>.</p>';

  foreach ($replacements as $search => $replace) {
    $contents = str_replace(
      $search,
      $replace,
      $contents
    );
  }

  // Save the file.
  file_put_contents($file, $contents);
}
