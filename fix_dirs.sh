# Get the directory back to gsac archiving.
gsacarchdir=$(pwd)

# Go to the archive directory.
cd ..
cd "$1"

# Remove the files and directories.
rm -rf search_modal
rm -rf cas?*
rm -rf forward?*
rm -rf graduate-program-search*
rm -rf graduate-course-search.html

# Go back to the gsac archiving directory.
cd "$gsacarchdir"