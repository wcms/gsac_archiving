<?php

/**
 * @file
 * Removing links, search and banners from previously archived sites.
 */

// Get the archive from the arguments.
$archive = $argv[1];

// Set the path to the archive.
$path = '../' . $archive;

// Reset the files array.
$files = [];

// Get the directories recursively.
$rdi = new RecursiveDirectoryIterator($path, FilesystemIterator::KEY_AS_PATHNAME);

// Step through each of the directories and get the files.
foreach (new RecursiveIteratorIterator($rdi, RecursiveIteratorIterator::SELF_FIRST) as $file => $info) {

  // If this is not a directory and not the . or .. file, then add
  // it to the files array.
  if (
    !is_dir($file) &&
    $info->getFileName() !== '.' &&
    $info->getFileName() !== '..'
  ) {

    $files[] = $file;
  }
}

// Step through each of the files and change the links.
foreach ($files as $file) {

  // Load the contents of the file.
  $contents = file_get_contents($file);

  // The links to be removed.
  $links = [
    'Graduate program search',
    'Graduate course search',
    'Archives',
  ];

  // Step through each of the links, and remove them.
  foreach ($links as $link) {
    $contents = preg_replace(
      '/[<li*>].*?' . $link . '\<\/a><\/li>/',
      '',
      $contents
    );
  }

  // Remove the search, since it does not work on flat files.
  $contents = preg_replace(
    '/<div id="uw-header-search-button[^>]*>(.*?)<\/div>/s',
    '',
    $contents
  );

  // Remove the banners.
  $contents = preg_replace(
    '/<div class=\"region region-banner-alt\">.*?<\/form>.*?<\/div>.*?<\/div>.*?<\/div>/s',
    '',
    $contents
  );

  // Save the file.
  file_put_contents($file, $contents);
}
